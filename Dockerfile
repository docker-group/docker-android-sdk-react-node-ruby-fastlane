FROM openjdk:8

ENV GRADLE_VERSION 2.14.1
ENV GRADLE_SDK_URL https://services.gradle.org/distributions/gradle-${GRADLE_VERSION}-bin.zip
ENV GRADLE_HOME /usr/local/gradle-${GRADLE_VERSION}
ENV PATH ${GRADLE_HOME}/bin:$PATH
ENV ANDROID_HOME /opt/android-sdk-linux
ENV PATH ${PATH}:${ANDROID_HOME}/tools:${ANDROID_HOME}/platform-tools:${ANDROID_HOME}/tools/bin
ENV LANG "C.UTF-8"
ENV LC_ALL "C.UTF-8"

# Install dependencies:
# Node 8
# zip
# 32-bit support for Android SDK
# Yarn
# Gradle
RUN apt-get update -q && \
    apt-get install -y apt-transport-https && \
    dpkg --add-architecture i386 && \
    curl -sS https://dl.yarnpkg.com/debian/pubkey.gpg | apt-key add - && \
    echo "deb https://dl.yarnpkg.com/debian/ stable main" | tee /etc/apt/sources.list.d/yarn.list && \    
    apt-get update -q && \
    echo "Installing zip" && \
    apt-get -y install zip expect && \
    echo "Installing Node" && \
    curl -sL https://deb.nodesource.com/setup_8.x | bash - && \
    apt-get install -y nodejs && \
    echo "Installing 32-bit support" && \
    apt-get install -qy --no-install-recommends libstdc++6:i386 libgcc1:i386 zlib1g:i386 libncurses5:i386 && \
    echo "Installing Yarn" && \
    apt-get install -y yarn && \
    echo "Installing ruby and rsync" && \
    apt-get install -y build-essential ruby ruby-dev rsync && \
    echo "Clean apt junk" && \
    rm -rf /var/lib/apt/lists/*
# Android SDK Tools
# Android SDK dependencies
RUN echo "Installing Android SDK Tools" && \
    cd /opt \
    && wget -q https://dl.google.com/android/repository/sdk-tools-linux-4333796.zip -O tools.zip \
    && mkdir -p ${ANDROID_HOME} \
    && unzip tools.zip -d ${ANDROID_HOME} \
    && rm -f tools.zip && \
    echo "Installing Android dependencies" && \
    mkdir -p $HOME/.android && \
    echo 'count=0' > $HOME/.android/repositories.cfg && \
    yes | $ANDROID_HOME/tools/bin/sdkmanager --licenses > /dev/null && \
    $ANDROID_HOME/tools/bin/sdkmanager \
        "tools" \
        "platform-tools" \
        "build-tools;23.0.1" \
        "build-tools;25.0.2" \
        "build-tools;26.0.1" \
        "build-tools;27.0.3" \
        "build-tools;28.0.3" \
        "platforms;android-23" \
        "platforms;android-25" \
        "platforms;android-26" \
        "platforms;android-27" \
        "platforms;android-28" && \
    (echo y; echo y; echo y; echo y; echo y; echo y) | sdkmanager --licenses --sdk_root=${ANDROID_HOME} && \
    $ANDROID_HOME/tools/bin/sdkmanager --update --verbose

# Fastlane
RUN echo "gem: --no-document" > ~/.gemrc
RUN gem install fastlane

#SSH and git configuration
RUN mkdir -p ~/.ssh \
  && touch ~/.ssh/id_rsa \
  && touch ~/.ssh/config \
  && chmod 700 ~/.ssh \
  && chmod 600 ~/.ssh/id_rsa

RUN printf "Host *\n\tStrictHostKeyChecking no\n\n" > ~/.ssh/config

RUN git config --global push.default simple  

WORKDIR /root